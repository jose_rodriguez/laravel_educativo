<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('nombre') }}
            {{ Form::text('nombre', $asignatura->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
            {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('titulación') }}
            {{ Form::text('titulación', $asignatura->titulación, ['class' => 'form-control' . ($errors->has('titulación') ? ' is-invalid' : ''), 'placeholder' => 'Titulación']) }}
            {!! $errors->first('titulación', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('creditos') }}
            {{ Form::text('creditos', $asignatura->creditos, ['class' => 'form-control' . ($errors->has('creditos') ? ' is-invalid' : ''), 'placeholder' => 'Creditos']) }}
            {!! $errors->first('creditos', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('max_alumnos') }}
            {{ Form::text('max_alumnos', $asignatura->max_alumnos, ['class' => 'form-control' . ($errors->has('max_alumnos') ? ' is-invalid' : ''), 'placeholder' => 'Max Alumnos']) }}
            {!! $errors->first('max_alumnos', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>