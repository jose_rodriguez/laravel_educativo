<div class="box box-info padding-1">
    <div class="box-body">

        <div class="form-group">
            {{ Form::label('Alumno') }}
            {{ Form::select('alumno_id', $alumnos, $matriculacione->alumno_id, ['class' => 'form-control' . ($errors->has('alumno_id') ? ' is-invalid' : ''), 'placeholder' => 'Alumno']) }}
            {!! $errors->first('alumno_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            {{ Form::label('Asignatura') }}
            {{ Form::select('asignatura_id', $asignaturas, $matriculacione->asignatura_id, ['class' => 'form-control' . ($errors->has('asignatura_id') ? ' is-invalid' : ''), 'placeholder' => 'Asignatura']) }}
            {!! $errors->first('asignatura_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            {{ Form::label('Año académico') }}
            {{ Form::text('anyo_academico', $matriculacione->anyo_academico, ['class' => 'form-control' . ($errors->has('anyo_academico') ? ' is-invalid' : ''), 'placeholder' => 'Año Académico']) }}
            {!! $errors->first('anyo_academico', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>