@extends('layouts.app')

@section('template_title')
    Matriculacione
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Matriculaciones') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('matriculaciones.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Añadir') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Alumno</th>
										<th>Asignatura</th>
										<th>Año Académico</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($matriculaciones as $matriculacione)
                                        <tr>
                                            <td>{{  $matriculacione->id }}</td>
                                            
											<td>{{ $matriculacione->alumno->nombre }}</td>
											<td>{{ $matriculacione->asignatura->nombre }}</td>
											<td>{{ $matriculacione->anyo_academico }}</td>

                                            <td>
                                                <form action="{{ route('matriculaciones.destroy',$matriculacione->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('matriculaciones.show',$matriculacione->id) }}"><i class="fa fa-fw fa-eye"></i> Mostrar</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('matriculaciones.edit',$matriculacione->id) }}"><i class="fa fa-fw fa-edit"></i> Editar</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Eliminar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $matriculaciones->links() !!}
            </div>
        </div>
    </div>
@endsection
