@extends('layouts.app')

@section('template_title')
    {{ $matriculacione->name ?? 'Show Matriculacione' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Matriculacione</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('matriculaciones.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Alumno Id:</strong>
                            {{ $matriculacione->alumno_id }}
                        </div>
                        <div class="form-group">
                            <strong>Asignatura Id:</strong>
                            {{ $matriculacione->asignatura_id }}
                        </div>
                        <div class="form-group">
                            <strong>Anyo Academico:</strong>
                            {{ $matriculacione->anyo_academico }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
