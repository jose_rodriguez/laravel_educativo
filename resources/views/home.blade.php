@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Panel de control') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="alert alert-info" role="alert">
                     {{ __('Estado de sesión: Inicio de sesión con normalidad.') }}
                    </div>

                    <div class="card">
                    <div class="card-header">
                        Enlaces del menú
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a class="nav-link" href="{{ route('alumnos.index') }}">{{ __('Alumnos') }}</a></li>
                        <li class="list-group-item"><a class="nav-link" href="{{ route('asignaturas.index') }}">{{ __('Asignaturas') }}</a></li>
                        <li class="list-group-item"><a class="nav-link" href="{{ route('convocatorias.index') }}">{{ __('Convocatorias') }}</a></li>
                        <li class="list-group-item"><a class="nav-link" href="{{ route('matriculaciones.index') }}">{{ __('Matriculaciones') }}</a></li>
                        <li class="list-group-item"><a class="nav-link" href="{{ route('calificaciones.index') }}">{{ __('Calificaciones') }}</a></li>
                    </ul>
                    </div>
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
