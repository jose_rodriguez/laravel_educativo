<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Matriculaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('matriculaciones', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id');
            $table->bigInteger('alumno_id')->unsigned();
            $table->bigInteger('asignatura_id')->unsigned();
            $table->integer('anyo_academico');
            $table->timestamps();

            //Claves foráneas:
            
            $table->foreign('alumno_id')->references('id')->on('alumnos')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('asignatura_id')->references('id')->on('asignaturas')
            ->onUpdate('cascade')->onDelete('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
