<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Calificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('convocatorias', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->timestamps();
        });
        
        Schema::create('calificaciones', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id');
            $table->bigInteger('alumno_id')->unsigned();
            $table->bigInteger('asignatura_id')->unsigned();
            $table->bigInteger('convocatoria_id')->unsigned();
            $table->float('calificacion',2, 2);
            $table->timestamps();


            $table->foreign('alumno_id')->references('id')->on('alumnos')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('asignatura_id')->references('id')->on('asignaturas')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('convocatoria_id')->references('id')->on('convocatorias')
            ->onUpdate('cascade')->onDelete('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
