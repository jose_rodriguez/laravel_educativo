<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) 
    {
        return view('home');
    }
    else
    {
        return view('auth.login');
    }
});

/** Carga home por defecto */
Route::get('/home', function () {
    if (Auth::check()) 
    {
        return view('home');
    }
    else
    {
        return view('auth.login');
    }
});

Auth::routes();

/** Acceso a la gestión de alumnos */
Route::resource('alumnos', App\Http\Controllers\AlumnoController::class)->middleware('auth');

/** Acceso a la gestión de asignaturas */
Route::resource('asignaturas', App\Http\Controllers\AsignaturaController::class)->middleware('auth');

/** Acceso a la gestión de convocatorias */
Route::resource('convocatorias', App\Http\Controllers\ConvocatoriaController::class)->middleware('auth');

/** Acceso a la gestión de matriculaciones */
Route::resource('matriculaciones', App\Http\Controllers\MatriculacioneController::class)->middleware('auth');

/** Acceso a la gestión de calificaciones */
Route::resource('calificaciones', App\Http\Controllers\CalificacioneController::class)->middleware('auth');