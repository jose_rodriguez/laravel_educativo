<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Calificacione
 *
 * @property $id
 * @property $alumno_id
 * @property $asignatura_id
 * @property $convocatoria_id
 * @property $calificacion
 * @property $created_at
 * @property $updated_at
 *
 * @property Alumno $alumno
 * @property Asignatura $asignatura
 * @property Convocatoria $convocatoria
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Calificacione extends Model
{
    
    static $rules = [
		'alumno_id' => 'required',
		'asignatura_id' => 'required',
		'convocatoria_id' => 'required',
		'calificacion' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['alumno_id','asignatura_id','convocatoria_id','calificacion'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function alumno()
    {
        return $this->hasOne('App\Models\Alumno', 'id', 'alumno_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function asignatura()
    {
        return $this->hasOne('App\Models\Asignatura', 'id', 'asignatura_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function convocatoria()
    {
        return $this->hasOne('App\Models\Convocatoria', 'id', 'convocatoria_id');
    }
    

}
