<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Matriculacione
 *
 * @property $id
 * @property $alumno_id
 * @property $asignatura_id
 * @property $anyo_academico
 * @property $created_at
 * @property $updated_at
 *
 * @property Alumno $alumno
 * @property Asignatura $asignatura
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Matriculacione extends Model
{
    
    static $rules = [
		'alumno_id' => 'required',
		'asignatura_id' => 'required',
		'anyo_academico' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['alumno_id','asignatura_id','anyo_academico'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function alumno()
    {
        return $this->hasOne('App\Models\Alumno', 'id', 'alumno_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function asignatura()
    {
        return $this->hasOne('App\Models\Asignatura', 'id', 'asignatura_id');
    }
    

}
