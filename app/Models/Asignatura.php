<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Asignatura
 *
 * @property $id
 * @property $nombre
 * @property $titulación
 * @property $creditos
 * @property $max_alumnos
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Asignatura extends Model
{
    
    static $rules = [
		'nombre' => 'required',
		'titulación' => 'required',
		'creditos' => 'required',
		'max_alumnos' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','titulación','creditos','max_alumnos'];



}
