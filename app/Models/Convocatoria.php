<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Convocatoria
 *
 * @property $id
 * @property $nombre
 * @property $created_at
 * @property $updated_at
 *
 * @property Calificacione[] $calificaciones
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Convocatoria extends Model
{
    
    static $rules = [
		'nombre' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calificaciones()
    {
        return $this->hasMany('App\Models\Calificacione', 'convocatoria_id', 'id');
    }
    

}
