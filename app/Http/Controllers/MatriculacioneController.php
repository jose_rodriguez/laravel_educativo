<?php

namespace App\Http\Controllers;

use App\Models\Matriculacione;
use App\Models\Alumno;
use App\Models\Asignatura;
use Illuminate\Http\Request;

/**
 * Class MatriculacioneController
 * @package App\Http\Controllers
 */
class MatriculacioneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matriculaciones = Matriculacione::paginate();

        return view('matriculacione.index', compact('matriculaciones'))
            ->with('i', (request()->input('page', 1) - 1) * $matriculaciones->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $matriculacione = new Matriculacione();
        $alumnos        = Alumno::pluck('nombre','id');
        $asignaturas    = Asignatura::pluck('nombre','id');
        return view('matriculacione.create', compact('matriculacione', 'alumnos' ,'asignaturas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Matriculacione::$rules);

        $matriculacione = Matriculacione::create($request->all());

        return redirect()->route('matriculaciones.index')
            ->with('success', 'Matriculacione created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $matriculacione = Matriculacione::find($id);

        return view('matriculacione.show', compact('matriculacione'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matriculacione = Matriculacione::find($id);
        $alumnos        = Alumno::pluck('nombre','id');
        $asignaturas    = Asignatura::pluck('nombre','id');
        return view('matriculacione.edit', compact('matriculacione','alumnos','asignaturas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Matriculacione $matriculacione
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Matriculacione $matriculacione)
    {
        request()->validate(Matriculacione::$rules);

        $matriculacione->update($request->all());

        return redirect()->route('matriculaciones.index')
            ->with('success', 'Matriculacione updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $matriculacione = Matriculacione::find($id)->delete();

        return redirect()->route('matriculaciones.index')
            ->with('success', 'Matriculacione deleted successfully');
    }
}
