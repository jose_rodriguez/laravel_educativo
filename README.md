<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2>
				Laravel Educativo
			</h2>
			<dl>
				<dt>
					Contexto
				</dt>
				<dd>
					Sistema informático educativo de gestión de alumnos y asignaturas creado a partir del framework Laravel y usando generador de CRUD : https://github.com/awais-vteams/laravel-crud-generator
				</dd>
				<dt>
					Requisitos
				</dt>
				<dd>
					Para este proyecto se necesitará de motor de base de datos MySQL.
				</dd>
				<dt>
					Creación de la base de datos
				</dt>
				<dd>
					La base de datos tiene por nombre "educativo". Se deberá crear la instancia con el siguiente comando ejecutado en línea de comandos:
				</dd>
				<code>CREATE DATABASE educativo >CHARACTER SET utf8 COLLATE utf8_general_ci;</code>
				<dt>
						Migración de las tablas
                </dt>
				<dd>
						Para la migración de las tablas necesarias para el uso de la aplicación deberá ejecutarse el siguiente comando:  <code>php artisan migrate</code>
                </dd>
                <dt>
					Primer acceso a la aplicación
				</dt>
				<dd>
					Al iniciar la app no existirán usuarios dados de alta. Se deberá dar de alta a un usuario y ya se podrán generar alumnos, asignaturas, matrículas, calificaciones y convocatorias así como realizar sus gestiones pertinentes.
				</dd>
			</dl>
		</div>
	</div>
</div>